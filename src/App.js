import "./App.css";
import { ShowCounter } from "./components/ShowCounter";

function App() {
  return (
    <div className="App">
      <header className="App-header">
        <ShowCounter />
      </header>
    </div>
  );
}

export default App;
