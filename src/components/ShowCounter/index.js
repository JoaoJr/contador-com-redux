import { useSelector, useDispatch } from "react-redux";
import { addNumber, subNumber } from "../../store/modules/counter/actions";

export const ShowCounter = () => {
  const dispatch = useDispatch();
  const number = useSelector((state) => state.number);

  const handleAdd = () => {
    dispatch(addNumber(1));
  };

  const handleSub = () => {
    dispatch(subNumber(1));
  };
  return (
    <div>
      <p>{number}</p>
      <button onClick={handleSub}>sub</button>
      <button onClick={handleAdd}>add</button>
    </div>
  );
};
