import { createStore, combineReducers } from "redux";
import counterReducer from "./modules/counter/reducer";

const reducers = combineReducers({ number: counterReducer });

const store = createStore(reducers);

export default store;
