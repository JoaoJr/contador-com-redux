import { ADD_NUMBER, SUB_NUMBER } from "./actionTypes";

const counterReducer = (state = 0, action) => {
  const { type, number } = action;
  switch (type) {
    case ADD_NUMBER:
      return state + number;

    case SUB_NUMBER:
      return state - number;

    default:
      return state;
  }
};
export default counterReducer;
